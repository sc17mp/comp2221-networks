import java.io.*;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server
{
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;

        try {
            serverSocket = new ServerSocket(8888);
            System.out.println("Listening at " + serverSocket.getLocalPort());
        } catch (IOException e) {
            System.err.println("Could not listen on port " + serverSocket.getLocalPort());
            System.exit(0);
        }

        ExecutorService threadpool = Executors.newFixedThreadPool(10);

        while (true) {
            Socket connectToClient = serverSocket.accept();
            threadpool.submit(new ClientHandler(connectToClient));
        }
    }
}