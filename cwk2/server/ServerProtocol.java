import java.io.*;
import java.net.*;

public class ServerProtocol {


    // list all file in the server directory
    public String listFile() {
        String currentDir;
        String filePath = "";
        try {
            currentDir = System.getProperty("user.dir");
            filePath = currentDir + "/serverFiles";
        } catch (NullPointerException d) {
            System.out.println("Cannot list files");

        }

        File serverPath = new File(filePath);
        File[] filesFromServer = serverPath.listFiles();
        String allFiles = "";

        for (File f: filesFromServer) {
            allFiles += f.getName()+"\n";
        }

        return allFiles;
    }

    // send a file requested by the client
    public void sendFile(Socket s, String fileName) {
        FileInputStream fileInput;
        BufferedInputStream bufferedInput;
        BufferedWriter output;

        try {
            File fileToSend = new File(System.getProperty("user.dir") + "/serverFiles/" + fileName);
            byte[] fileArray = new byte[(int) fileToSend.length()];

            fileInput = new FileInputStream(fileToSend);

            long fileSize = fileToSend.length();
            // write instruction to server
            BufferedWriter requestToServer = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
            requestToServer.write(String.valueOf(fileSize));
            requestToServer.newLine();
            requestToServer.flush();

            bufferedInput = new BufferedInputStream(fileInput);
            bufferedInput.read(fileArray, 0, fileArray.length);

            output = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
            System.out.println("Sending " + fileName);

            for (int i = 0; i < fileArray.length; i++) {
                output.write(fileArray[i]);
            }
            output.flush();
            System.out.println("File sent");

            output.close();
            bufferedInput.close();

        } catch (FileNotFoundException f) {
            System.err.println("File not found");

        } catch (IOException e) {
            System.err.println("Error while sending file");
        }

    }

    // save a file sent from server
    public void saveFile(Socket s,String fileName) {
        InputStream input;
        OutputStream output;

        try {
            input = s.getInputStream();
            output = new FileOutputStream(System.getProperty("user.dir") + "/serverFiles/" + fileName);

            BufferedReader fromClient = new BufferedReader(new InputStreamReader(s.getInputStream()));
            String fileSize = fromClient.readLine();

            int size = Integer.parseInt(fileSize);
            byte[] fileReceived = new byte[size];

            int bytesRead;
            while ((bytesRead = input.read(fileReceived)) > 0) {
                output.write(fileReceived, 0, bytesRead);
            }

            output.flush();
            output.close();
        } catch (IOException e) {
            System.err.println("Error while saving file");
        }
    }

}