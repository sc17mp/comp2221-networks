import java.io.*;
import java.net.*;
import java.util.*;


public class ClientHandler extends Thread {
    private Socket connectToClient = null;

    public ClientHandler(Socket connectToClient) {
        this.connectToClient = connectToClient;
    }
    public void run() {
        String serverOutput;
        String message;
        ServerProtocol protocol = new ServerProtocol();
        try {
            while(true) {
                InetAddress inet = connectToClient.getInetAddress();
                Date date = new Date();
                System.out.println("\nDate " + date.toString() );
                System.out.println("Connection made from " + inet.getHostName() );

                // read from command line of client
                BufferedReader fromClient = new BufferedReader(new InputStreamReader(connectToClient.getInputStream()));
                if ((message = fromClient.readLine()) != null) System.out.println("Request: " + message);

                if (message.equalsIgnoreCase("list")) {
                    System.out.println("Listing out all the files");
                    PrintWriter writer = new PrintWriter(new OutputStreamWriter(connectToClient.getOutputStream()));
                    serverOutput = protocol.listFile();
                    writer.write(serverOutput);
                    writer.flush();
                    writer.close();
                } else if (message.contains("get")) {
                    System.out.println("The get command");
                    String fileName = message.substring(3,message.length());
                    protocol.sendFile(connectToClient,fileName);
                } else if (message.contains("put")) {
                    System.out.println("The put command");
                    String fileName = message.substring(3,message.length());
                    protocol.saveFile(connectToClient,fileName);
                } else System.out.println(message + " is an invalid instruction");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
