import java.io.*;
import java.net.*;
import java.util.*;

public class Client {
    private Socket clientSocket;

    // send a file requested to the server
    public void sendFile(String fileName) {
        FileInputStream fileInput;
        BufferedInputStream bufferedInput;
        DataOutputStream output;
        try {
                // read the file locally
            File fileToSend = new File(System.getProperty("user.dir") + "/clientFiles/" + fileName);
            byte [] fileArray = new byte[(int)fileToSend.length()];

            fileInput = new FileInputStream(fileToSend);
            bufferedInput = new BufferedInputStream(fileInput);
            bufferedInput.read(fileArray, 0, fileArray.length);

            output = new DataOutputStream(clientSocket.getOutputStream());
            System.out.println("Sending " + fileName);

            output.write(fileArray, 0, fileArray.length);
            output.flush();
            clientSocket.close();

        } catch (FileNotFoundException f) {
            System.out.print("File does not exist");
        } catch (IOException e) {
            System.out.print("Error while transferring file");
        }
    }

    // save a file sent from server
    public void saveFile(String fileName) {
        InputStream input;
        OutputStream output;

        try {
            input = clientSocket.getInputStream();
            output = new FileOutputStream(System.getProperty("user.dir") + "/clientFiles/" + fileName);

            BufferedReader fromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String fileSize = fromClient.readLine();

            int size = Integer.parseInt(fileSize);
            byte[] fileReceived = new byte[size];

            int bytesRead;
            while ((bytesRead = input.read(fileReceived)) > 0) {
                output.write(fileReceived, 0, bytesRead);
            }

            output.flush();
            output.close();
        } catch (IOException e) {
            System.err.println("Error while saving file");
        }
    }

    public void connectServer(String request) {
        String message = "";
        try {
            // run the server on the same machine as the client
            clientSocket = new Socket("localhost", 8888);


            // write instruction to server
            PrintWriter requestToServer = new PrintWriter(clientSocket.getOutputStream(), true);
            requestToServer.println(request);

            // read from server
            if (request.equalsIgnoreCase("list")) {
                BufferedReader fromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                while ((message = fromServer.readLine())!=null) System.out.println(message);
                fromServer.close();
            } else if (request.contains("get")) {
                String fileName = request.substring(3,request.length());
                System.out.println("Saving " + fileName +" from client");
                this.saveFile(fileName);
                System.out.println("File is saved");
            } else if (request.contains("put")) {
                String fileName = request.substring(3,request.length());
                System.out.println("Sending " + fileName + " to client");
                this.sendFile(fileName);
            }

            clientSocket.close();

        } catch (IOException e) {
            System.err.println("Cannot connect to server");
        }
    }
    public static void main( String[] args ) {
        if (args.length == 0) {
            System.out.println("No request input, exit");
            System.exit(0);
        }
        String clientRequest = "";

        for (int i = 0; i<args.length; i++) clientRequest += args[i];
        Client client = new Client();
        client.connectServer(clientRequest);
    }
}
